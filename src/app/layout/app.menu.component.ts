import { Component, OnInit } from '@angular/core';
import { LayoutService } from './service/app.layout.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[] = [];

    constructor(public layoutService: LayoutService) { }

    ngOnInit() {
        this.model = [
            {
                label: 'Anagrafica',
                items: [
                    { label: 'Elenco', icon: 'pi pi-fw pi-home', routerLink: ['/'] },
                    { label: 'Nuovo', icon: 'pi pi-fw pi-id-card', routerLink: ['/customer'] },
                ]
            }
        ];
    }
}
