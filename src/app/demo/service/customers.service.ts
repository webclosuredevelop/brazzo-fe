import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { baseUrl } from './constant';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private http: HttpClient) { }

  all() {
    return this.http.get(baseUrl + 'customer/all?first=0&rows=300')
  }

  get(id: number) {
    return this.http.get(baseUrl + 'customer/' + id)
  }

  save(request: any) {
    return this.http.post(baseUrl + 'customer/update', request)
  }

  delete(id: number) {
    return this.http.delete(baseUrl + 'customer/' + id)
  }

}
