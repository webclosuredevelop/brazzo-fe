import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  downloadFile() {
    const url = 'http://localhost:8000/uploads/release/6539398804d7c.xlsx';
    return this.http.get(url, { responseType: 'blob' });
  }

}