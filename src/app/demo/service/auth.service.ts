import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, lastValueFrom, map } from "rxjs";
import { baseUrl } from "./constant";

// Auth service
@Injectable({ providedIn: 'root' })
export class AuthService {
    private _loginUrl = baseUrl + "login";
    private userDetails = null;
    private refreshInterval = null;
    constructor(private _http: HttpClient) { }
    // login method
    Login(email: string, password: string): Observable<any> {
        if (this.refreshInterval != null) {
            clearInterval(this.refreshInterval);
            this.refreshInterval = null;
        }
        return this._http.post(this._loginUrl, { email, password }).pipe(
            map(async (response: any) => {
                localStorage.setItem('jwt', JSON.stringify(response));
                this.userDetails = await lastValueFrom(this._http.get(baseUrl + "user"));
                localStorage.setItem('user', JSON.stringify(this.userDetails));
                const now = new Date();
                return response;
            })
        );
    }

    getAuthorizationToken(): any {
        let jwt: any = localStorage.getItem("jwt");
        console.log(jwt);
        if (jwt != undefined && jwt != null) {
            jwt = JSON.parse(jwt);
            return jwt.access_token;
        } else {
            return null;
        }
    }

    isLoggedIn(): Observable<any> {
        console.trace();
        return new Observable((x) => {
            const user = localStorage.getItem("user");
            if (user != null && user != undefined) {
                x.next(true);
            } else {
                x.next(false);
            }
        });
    }

    // You need to make a request to the server as well
    Logout() {
        this._http.post(baseUrl + "logout", {}).subscribe(async response => {
            console.log("Logout: ", response);

        })
        if (this.refreshInterval != null) {
            clearInterval(this.refreshInterval);
            this.refreshInterval = null;
        }
        localStorage.removeItem("user");
        localStorage.removeItem("jwt");
    }
}