import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent {
  form: FormGroup

  dateStart = new FormControl(new Date())
  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      name: new FormControl(null),
      surname: new FormControl(null),
      codice_fiscale: new FormControl(null),
      indirizzo: new FormControl(null),
      civico: new FormControl(null),
      comune: new FormControl(null),
      CAP: new FormControl(null),
      provincia: new FormControl(null),
      data_nascita: new FormControl(null),
      email: new FormControl(null),
      phone: new FormControl(null),
      category: new FormControl(null),
      dateStart: new FormControl(),
      last_visit: new FormControl(),
      monitoring: new FormControl(false),
      // for example this field next is not found in form
      next: new FormControl(),
      urgente: new FormControl(false),
      urgency: new FormControl(null),
      message: new FormControl(null),
      note: new FormControl(null),
      score: new FormControl(0),
    })
  }
}
